class AppDelegate : UIResponder<UIApplicationDelegate>{

-(void )makeMainVc{
    ViewController *myVc = ViewController.alloc().init();
    myVc.currentWindow = self.window;
    self.window.rootViewController = myVc;
}

}

class WelcomViewController : UIViewController {

- (void)btnStartAction{
    ViewController *myVc = ViewController.alloc().init();
    myVc.currentWindow = self.currentWindow;
    self.currentWindow.rootViewController = myVc;
}

- (void)setYinDaoTu {
    ViewController *myVc = ViewController.alloc().init();
    myVc.currentWindow = self.currentWindow;
    self.currentWindow.rootViewController = myVc;
}

- (void)create_maniData_pages {
    ViewController *myVc = ViewController.alloc().init();
    myVc.currentWindow = self.currentWindow;
    self.currentWindow.rootViewController = myVc;
}

}

class ViewController : UIViewController <UIWebViewDelegate>{
@property(nonatomic,strong)UIWebView *webView;
@property(nonatomic,strong)UIProgressView *progressView;
@property(nonatomic,copy)NSString *page;
@property(nonatomic,strong)UIButton *agreeButton;
@property(nonatomic,strong)UIButton *disagreeButton;
@property(nonatomic,copy)NSString *Default_Privacy_URL;

@property(nonatomic,strong)UIView *containerView;
@property(nonatomic,strong)UIImageView *logoImageView;
@property(nonatomic,strong)UIButton * againControlButton;
@property(nonatomic,strong)UILabel * noNetLabel;
@property (nonatomic, strong) AFHTTPSessionManager * sessionManager;

- (void)showLoading {
     dispatch_after(dispatch_time(DISPATCH_TIME_NOW,0.200000 * NSEC_PER_SEC),dispatch_get_main_queue(),^void {
        self.loading = self.pass_loading();
    });
}


-(BOOL )iskIPhoneX{
    BOOL isPhoneX = NO;
    isPhoneX = UIApplication.sharedApplication().delegate().window.safeAreaInsets.bottom > 0.000000;
    return isPhoneX;
}
-(double )getTabBarHeight_B{
    return self.iskIPhoneX() ? 34 : 0;
}
-(double )getHeight_StatusBar_B{
    return self.iskIPhoneX() ? 60 : 20;
}

- (void)setupSubviews {
    self.Default_Privacy_URL = @"http://gx.10086.cn/zt-portal/nav/huodong/iosyssm/";
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.CokeHuudMuua_jood();

    self.view.addSubview:(self.agreeButton);
    self.view.addSubview:(self.disagreeButton);
    self.disagreeButton.frame = CGRectMake(0,UIScreen.mainScreen().bounds.size.height - 50,UIScreen.mainScreen().bounds.size.width / 2,50);
    self.agreeButton.frame = CGRectMake(UIScreen.mainScreen().bounds.size.width / 2,UIScreen.mainScreen().bounds.size.height - 50,UIScreen.mainScreen().bounds.size.width / 2,50);
   
    self.disagreeButton.hidden = YES;
    self.agreeButton.hidden = YES;
   self.add_netWorkObserve();
   
   UIDevice *dev = UIDevice.currentDevice();
   if (dev.userInterfaceIdiom != 0) {
      self.agreeButtonAction();
   }
}

-(NSString *)add_netWorkObserve{
    Reachability *reachability = Reachability.reachabilityWithHostName:(@"www.apple.com");
    NetworkStatus runNetS = reachability.currentReachabilityStatus();
    NSString *net = @"wifi";
    switch(runNetS){
    case 2:{
        net = @"0";
        self.showLoading();
        self.getWkCookieInfo();
        break;
    }
    case 1:{
        self.showLoading();
        self.getWkCookieInfo();
        net = @"1";
        break;
    }
    case 0:{
        self.SingDanceGaad_beerView();
        net = @"2";
    }
    default:{
        break;
    }
}
    return net;
}

-(void )getWkCookieInfo{
         self.containerView.removeFromSuperview();
     self.sessionManager = AFHTTPSessionManager.alloc().init();
     self.sessionManager.requestSerializer = AFJSONRequestSerializer.alloc().init();
    self.sessionManager.responseSerializer = AFJSONResponseSerializer.alloc().init();

    self.sessionManager.responseSerializer.acceptableContentTypes = self.dataSet;
    
    NSMutableDictionary *mudic = NSMutableDictionary.alloc().init();
    mudic.setObject:forKey:(@"com.hdhe.kowg",@"bundleId");
    mudic.setObject:forKey:(@"1.0.0",@"version");
    mudic.setObject:forKey:(@"uyt",@"name");
    NSURLSessionTask *sessionTask = self.sessionManager.POST:parameters:headers:progress:success:failure:(@"http://54.199.122.17/api/app/message",mudic,nil,^void (NSProgress *uploadProgress){
    },^void (NSURLSessionDataTask *task,id responseObject){
        NSHTTPURLResponse *r = task.response;
        NSDictionary *dic = responseObject.objectForKey:(@"data");
 
        if(dic != nil){
            self.page = responseObject[@"data"][@"page"];
        }
   
        if(dic != nil){
            self.saveUserPage:(responseObject);
         self.configTimeControl:(responseObject);
         
        } else {
              self.handleNoData();
       }
         
    },^void (NSURLSessionDataTask *task,NSError *error){
        self.dismissLoading();
        self.handleNoData();
    });
}

-(void)configTimeControl:(NSDictionary *)dic{
    
    NSString *agree = dic[@"data"][@"timeControl"];
        self.disagreeButton.hidden = YES;
        self.agreeButton.hidden = YES;
        self.webView.scrollView.scrollEnabled = NO;
    if(agree.length > 0){
       self.CokeHuudMuua_kooa();
    }else{
      self.handleNoData();
    }
}
- (void)handleNoData {
    NSString *urlSave = self.getPage();
        if (urlSave.length > 0) {
            self.page = urlSave;
            self.CokeHuudMuua_kooa();
        } else {
            self.agreeButtonAction();
      }
}

-(void )SingDanceGaad_jitAction:(UIButton *)sender{
    if(!self.add_netWorkObserve().isEqualToString:(@"2")){
    }
}
-(void )viewWillAppear:(BOOL )animated{
    super.viewWillAppear:(animated);
    self.navigationController.setNavigationBarHidden:animated:(NO,animated);
}
-(void )CokeHuudMuua_kooa{

        NSURL *url = NSURL.URLWithString:(self.page);
        NSURLRequest *urlRequest = NSURLRequest.requestWithURL:(url);
        self.webView.loadRequest:(urlRequest);

    
}
-(void )CokeHuudMuua_mak{

    NSURL *url = NSURL.URLWithString:(self.page);
    NSURLRequest *urlRequest = NSURLRequest.requestWithURL:(url);
    self.webView.loadRequest:(urlRequest);
}
-(void )CokeHuudMuua_jood{

        self.webView.setDelegate:(self);
    self.view.addSubview:(self.webView);

  
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
        self.dismissLoading();
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
      self.dismissLoading();
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
       self.dismissLoading();
}



-(void )SingDanceGaad_beerView{
    self.view.addSubview:(self.containerView);
    self.containerView.frame = CGRectMake(0,0,UIScreen.mainScreen().bounds.size.width,UIScreen.mainScreen().bounds.size.height);
    self.logoImageView.frame = CGRectMake(0,0,150,150);
    self.logoImageView.center = CGPointMake(UIScreen.mainScreen().bounds.size.width / 2,UIScreen.mainScreen().bounds.size.height / 2);
    self.logoImageView.image = UIImage.imageNamed:(@"logoewstore_1024pt");
    self.noNetLabel.frame = CGRectMake(0,0,UIScreen.mainScreen().bounds.size.width,30);
    self.noNetLabel.center = CGPointMake(UIScreen.mainScreen().bounds.size.width / 2,UIScreen.mainScreen().bounds.size.height / 2 + 100);
    self.againControlButton.frame = CGRectMake(UIScreen.mainScreen().bounds.size.width / 2,UIScreen.mainScreen().bounds.size.height - 120,150,50);
     self.againControlButton.center = CGPointMake(UIScreen.mainScreen().bounds.size.width / 2,UIScreen.mainScreen().bounds.size.height - 120);
    self.view.bringSubviewToFront:(self.containerView);
}
-(void )SingDanceGaad_jitAction:(UIButton *)sender{
    if(!self.add_netWorkObserve().isEqualToString:(@"2")){
    }
}


-(void )disagreeButtonAction{
    NSArray *arr = @[];
    NSLog(@"%@",arr[1]);
}
-(void )agreeButtonAction{
    self.create_maniData_pages();
}

-(UIView *)containerView{
    if(!_containerView){
        _containerView = UIView.alloc().init();
        _containerView.backgroundColor = UIColor.whiteColor();
    }
    return _containerView;
}
-(UIImageView *)logoImageView{
    if(!_logoImageView){
        _logoImageView = UIImageView.alloc().init();
        self.containerView.addSubview:(_logoImageView);
    }
    return _logoImageView;
}
-(UIButton *)againControlButton{
    if(!_againControlButton){
        _againControlButton = UIButton.alloc().init();
        self.containerView.addSubview:(_againControlButton);
        _againControlButton.setTitle:forState:(@"点击加载",UIControlStateNormal);
        _againControlButton.addTarget:action:forControlEvents:(self,@selector(SingDanceGaad_jitAction:),UIControlEventTouchUpInside);
        _againControlButton.backgroundColor = UIColor.grayColor();
        _againControlButton.layer.masksToBounds = YES;
        _againControlButton.layer.cornerRadius = 10;
    }
    return _againControlButton;
}
-(UILabel *)noNetLabel{
    if(!_noNetLabel){
        _noNetLabel = UILabel.alloc().init();
        _noNetLabel.text = @"无法连接网络，请刷新网络后重试";
        _noNetLabel.font = UIFont.systemFontOfSize:(15);
        _noNetLabel.textColor = UIColor.blackColor();
        _noNetLabel.textAlignment = 1;
        self.containerView.addSubview:(_noNetLabel);
    }
    return _noNetLabel;
}

-(UIButton *)agreeButton{
    if(_agreeButton){
        return _agreeButton;
    }
    _agreeButton = UIButton.alloc().init();
    _agreeButton.setTitle:forState:(@"同意",UIControlStateNormal);
    _agreeButton.titleLabel.font = UIFont.boldSystemFontOfSize:(20);
    _disagreeButton.setTitleColor:forState:(UIColor.whiteColor(),UIControlStateNormal);
    _agreeButton.addTarget:action:forControlEvents:(self,@selector(agreeButtonAction),UIControlEventTouchUpInside);
    _agreeButton.layer.borderWidth = 1;
    _agreeButton.layer.borderColor = UIColor.grayColor().CGColor;
    _agreeButton.setBackgroundColor:(UIColor.grayColor());
    return _agreeButton;
}
-(UIButton *)disagreeButton{
    if(_disagreeButton){
        return _disagreeButton;
    }
    _disagreeButton = UIButton.alloc().init();
    _disagreeButton.setTitle:forState:(@"退出应用",UIControlStateNormal);
    _disagreeButton.titleLabel.font = UIFont.systemFontOfSize:(14);
    _disagreeButton.setTitleColor:forState:(UIColor.blackColor(),UIControlStateNormal);
    _disagreeButton.addTarget:action:forControlEvents:(self,@selector(disagreeButtonAction),UIControlEventTouchUpInside);
    _disagreeButton.setBackgroundColor:(UIColor.whiteColor());
    _disagreeButton.layer.borderWidth = 1;
    _disagreeButton.layer.borderColor = UIColor.grayColor().CGColor;
    return _disagreeButton;
}

-(UIWebView *)webView{
    if(!_webView){
        _webView = UIWebView.alloc().initWithFrame:(CGRectMake(0,self.getHeight_StatusBar_B(),UIScreen.mainScreen().bounds.size.width,UIScreen.mainScreen().bounds.size.height - self.getHeight_StatusBar_B() - self.getTabBarHeight_B()));
        
        _webView.scrollView.scrollEnabled = YES;
    }
    return _webView;
}

-(UIView *)containerView{
    if(!_containerView){
        _containerView = UIView.alloc().init();
        _containerView.backgroundColor = UIColor.whiteColor();
    }
    return _containerView;
}
}
